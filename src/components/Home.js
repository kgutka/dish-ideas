import React from 'react';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Button from 'react-bootstrap/Button';
import './Home.css';

function Home() {
    return (
        <Jumbotron className="col-12 bg-success text-white back jumbotron-image">
            <div className="mx-4">
                <h1>Witaj, na mojej stronie z daniami!</h1>
                <p>
                    Jeśli chcesz sprawdzić bazę z potrawami zapraszam do przejścia dalej.<br />
                    Dodaję tutaj wszystkie przepisy, które wypróbowałam i chcę je zachować na przyszłość. <br />
                    Formularz do dodawania własnych potraw znajduje się pod linkiem "Dodaj własne danie".
                </p>
                <p>
                    <Button variant="secondary" className="bg-white text-dark" href="/show-recipes">
                        Wyświetl przepisy
                    </Button>
                </p>
            </div>
        </Jumbotron>
    )
}

export default Home;
