import React from 'react';
import AddRecipeForm from "./AddRecipeForm";
import ShowRecipes from './ShowRecipes';
import Breakfast from './Breakfast';
import Lunch from "./Lunch";
import Dessert from './Dessert';
import Supper from './Supper';
import Pastries from "./Pastries";
import CalculateVolume from './CalculateVolume';
import Home from './Home';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Button from 'react-bootstrap/Button';
import './Navmenu.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import { NavDropdown } from 'react-bootstrap';

function Navmenu() {

  return (


    <div>
      <Router>
        <Navbar bg="dark" variant="dark">
          <Navbar.Brand href="#home"><Link to="/">Przepisiarnia</Link></Navbar.Brand>

          <Nav className="mr-auto">
            <NavDropdown title="Przepisy" id="choose-recipes-to-show">
              <NavDropdown.Item><Link to="/show-recipes">Wszystkie przepisy</Link></NavDropdown.Item>
              <NavDropdown.Item><Link to="/breakfast">Śniadania</Link></NavDropdown.Item>
              <NavDropdown.Item><Link to="/lunch">Lunche</Link></NavDropdown.Item>
              <NavDropdown.Item><Link to="/dessert">Desery</Link></NavDropdown.Item>
              <NavDropdown.Item><Link to="/supper">Obiady</Link></NavDropdown.Item>
              <NavDropdown.Item><Link to="/pastries">Wypieki</Link></NavDropdown.Item>
            </NavDropdown>
            <Nav.Link><Link to="/add-recipe-form">Dodaj własne danie</Link></Nav.Link>
            <Nav.Link><Link to="/calculate-volume">Przelicz objętość</Link></Nav.Link>
          </Nav>
          <Form inline>
            <FormControl type="text" placeholder="Szukaj" className="mr-sm-2" />
            <Button variant="outline-info">Szukaj</Button>
          </Form>
        </Navbar>
        <Switch>
          <Route path="/show-recipes">
            <ShowRecipes />
          </Route>
          <Route path="/breakfast">
            <Breakfast />
          </Route>
          <Route path="/lunch">
            <Lunch />
          </Route>
          <Route path="/dessert">
            <Dessert />
          </Route>
          <Route path="/supper">
            <Supper />
          </Route>
          <Route path="/pastries">
            <Pastries />
          </Route>
          <Route path="/add-recipe-form">
            <AddRecipeForm />
          </Route>
          <Route path="/calculate-volume">
            <CalculateVolume />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}
export default Navmenu;