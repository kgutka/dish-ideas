import React, { useState, useEffect } from 'react';
import './Breakfast.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Table } from 'react-bootstrap';

function Breakfast() {
    const [recipes, setRecipes] = useState([]);
    const fetchRecipes = () => {
        fetch('http://192.168.1.68:5000/recipes')
        .then((res) => res.json())
        .then((res) => setRecipes(res))
    }
    useEffect(() => {
        fetchRecipes();
    },[]);
    
    return (
        <div className="row pt-5 px-5">
            <div className="col-2" />
            <div className="col-8">
                <Table stripped  hover>
                    <thead>
                        <tr>
                            <th>Nazwa</th>
                            <th>Link</th>
                            <th>Komentarz</th>
                            <th>Data spożycia</th>
                        </tr>
                    </thead>
                    <tbody>
                        {recipes.map((recipe) => {
                            return (
                                <tr>
                                    <td>{recipe.name}</td>
                                    <td>{recipe.link}</td>
                                    <td>{recipe.comment}</td>
                                    <td>{recipe.estingDate}</td>
                                </tr>
                            )
                            }
                        )}
                    </tbody>
                

                </Table>
                

            </div>
            <div className="col-2" />
            
        </div>
    );
}
export default Breakfast;