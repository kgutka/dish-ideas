import React, { useState } from 'react';
import Form from 'react-bootstrap/Form';
import Table from 'react-bootstrap/Table';
import './CalculateVolume.css';


function CalculateVolume() {
    const [dataToConvert, setDataToConvert] = useState({
        ingredient: "sugar",
        amount: 0,
        typeOfAmount: "gram",
        resultType: "gram",
    });

    const Convert = () => {
        const { amount } = dataToConvert;
        switch (dataToConvert.resultType) {
            case "gram":
                switch (dataToConvert.ingredient) {
                    case "sugar":
                        switch (dataToConvert.typeOfAmount) {
                            case "glass":
                                return amount * 240 + " gram";
                            case "gram":
                                return amount + " gram";
                            // return amount * 0.00385 });
                            case "mililiter":
                                return amount * 1.04 + " gram";
                            case "spoon":
                                return amount * 14 + " gram";
                            case "teaspoon":
                                return amount * 5 + " gram";
                            case "gram":
                                return amount + " gram";
                        }


                    case "rice":
                        switch (dataToConvert.typeOfAmount) {
                            case "glass":
                                return amount * 200;
                            case "gram":
                                return amount;
                            case "mililiter":
                                return amount / 200 * 250;
                            case "spoon":
                                return amount * 19 + " gram";
                            case "teaspoon":
                                return amount * 7 + " gram";
                            case "gram":
                                return amount + " gram";
                        }


                    case "groats":
                        switch (dataToConvert.typeOfAmount) {
                            case "glass":
                                return amount * 187 + " gram";
                            case "gram":
                                return amount + " gram";
                            case "mililiter":
                                return amount * 187 / 250 + " gram";
                            case "spoon":
                                return amount * 19 + " gram";
                            case "teaspoon":
                                return amount * 6 + " gram";
                            case "gram":
                                return amount + " gram";
                        }

                }


            case "mililiter":
                switch (dataToConvert.ingredient) {
                    case "sugar":
                        switch (dataToConvert.typeOfAmount) {
                            case "glass":
                                return amount * 250 + " ml";
                            case "gram":
                                return amount / 1.04 + " ml";
                            case "mililiter":
                                return amount + " ml";
                            case "spoon":
                                return amount * 15 + " ml";
                            case "teaspoon":
                                return amount * 4.9 + " ml";
                            case "gram":
                                return "Nieprzydatne";
                        }


                    case "rice":
                        switch (dataToConvert.typeOfAmount) {
                            case "glass":
                                return amount * 250 + " ml";
                            case "gram":
                                return "Nieprzydatne";
                            case "mililiter":
                                return amount + " ml";
                            case "spoon":
                                return amount / 15 + " ml";
                            case "teaspoon":
                                return amount / 5 + " ml";
                            case "gram":
                                return "Niepotrzebne";
                        }


                    case "groats":
                        switch (dataToConvert.typeOfAmount) {
                            case "glass":
                                return "Niepotrzebne";
                            case "gram":
                                return "Niepotrzebne";
                            case "mililiter":
                                return "Niepotrzebne";
                            case "spoon":
                                return "Niepotrzebne";
                            case "teaspoon":
                                return "Niepotrzebne";
                            case "gram":
                                return "Niepotrzebne";
                        }

                }

            case "Łyżeczki":
                switch (dataToConvert.ingredient) {
                    case "Cukier":
                        switch (dataToConvert.typeOfAmount) {
                            case "glass":
                                return "Niepotrzebne";
                            case "gram":
                                return "Niepotrzebne";
                            case "mililiter":
                                return "Niepotrzebne";
                            case "spoon":
                                return "Niepotrzebne";
                            case "teaspoon":
                                return "Niepotrzebne";
                            case "gram":
                                return "Niepotrzebne";
                        }


                    case "rice":
                        switch (dataToConvert.typeOfAmount) {
                            case "glass":
                                return "Niepotrzebne";
                            case "gram":
                                return "Niepotrzebne";
                            case "mililiter":
                                return "Niepotrzebne";
                            case "spoon":
                                return "Niepotrzebne";
                            case "teaspoon":
                                return "Niepotrzebne";
                            case "gram":
                                return "Niepotrzebne";
                        }


                    case "groats":
                        switch (dataToConvert.typeOfAmount) {
                            case "glass":
                                return "Niepotrzebne";
                            case "gram":
                                return "Niepotrzebne";
                            case "mililiter":
                                return "Niepotrzebne";
                            case "spoon":
                                return "Niepotrzebne";
                            case "teaspoon":
                                return "Niepotrzebne";
                            case "gram":
                                return "Niepotrzebne";
                        }

                }

            case "spoon":
                switch (dataToConvert.ingredient) {
                    case "sugar":
                        switch (dataToConvert.typeOfAmount) {
                            case "glass":
                                return "Niepotrzebne";
                            case "gram":
                                return "Niepotrzebne";
                            case "mililiter":
                                return "Niepotrzebne";
                            case "spoon":
                                return "Niepotrzebne";
                            case "teaspoon":
                                return "Niepotrzebne";
                            case "gram":
                                return "Niepotrzebne";
                        }


                    case "rice":
                        switch (dataToConvert.typeOfAmount) {
                            case "glass":
                                return "Niepotrzebne";
                            case "gram":
                                return "Niepotrzebne";
                            case "mililiter":
                                return "Niepotrzebne";
                            case "spoon":
                                return "Niepotrzebne";
                            case "teaspoon":
                                return "Niepotrzebne";
                            case "gram":
                                return "Niepotrzebne";
                        }


                    case "groats":
                        switch (dataToConvert.typeOfAmount) {
                            case "glass":
                                return "Niepotrzebne";
                            case "gram":
                                return "Niepotrzebne";
                            case "mililiter":
                                return "Niepotrzebne";
                            case "spoon":
                                return "Niepotrzebne";
                            case "teaspoon":
                                return "Niepotrzebne";
                            case "gram":
                                return "Niepotrzebne";
                        }

                }

        }
    }


    return (
        <div className="col-12 m-3 calculate-volume">
            <h1 className="col-12 4 text-center calculate-volume-header mb-5 text-uppercase">Przelicz objętość</h1>
            <div className="row font-weight-light">
                <div className="col-4 calculate-volume-info">
                    <h3 className="h4 text-uppercase font-weight-light title">Podstawowe informacje</h3>
                    1 glass = 250 ml<br />
                    kostka masła= 250 g<br />
                    kostka drożdzy = 100 gram<br />
                    drożdzy świeżych dodajemy 2 x więcej niż suchych
                </div>
                <div className="col-4">
                    <ol>
                        <h3 className="h4 text-uppercase font-weight-light title"><li>Wybiesz składnik:</li></h3>
                        <Form id="calculate-volume-form">
                            <Form.Control as="select" value={dataToConvert.ingredient} onChange={(change) => setDataToConvert({ ...dataToConvert, ingredient: change.target.value })} className="mb-3" id="choseTypeOfVolumeToCalculate">
                                <option value="sugar">Cukier</option>
                                <option value="rice">Ryż</option>
                                <option value="groats">Kasza</option>
                            </Form.Control>

                            <h3 className="h4 text-uppercase font-weight-light title"><li>Wpisz ilość:</li></h3>
                            <div className="same-line">
                                <Form.Control type="text" value={dataToConvert.amount} onChange={(change) => setDataToConvert({ ...dataToConvert, amount: change.target.value })} className="same-line volume mr-1" />
                            </div>
                            <div className="same-line">
                                <Form.Control as="select" value={dataToConvert.typeOfAmount} className="typeOfAmount same-line" onChange={(change) => setDataToConvert({ ...dataToConvert, typeOfAmount: change.target.value })}>
                                    <option value="glass">Szklanka</option>
                                    <option value="gram">Gram</option>
                                    <option value="mililiter">Mililitr</option>
                                    <option value="teaspoon">Łyżeczka</option>
                                    <option value="spoon">Łyżka</option>
                                </Form.Control>
                            </div>
                            <h3 className="h4 text-uppercase font-weight-light title"><li>Przelicz na:</li></h3>
                            <Form.Control as="select" value={dataToConvert.resultAmount} onChange={(change) => setDataToConvert({ ...dataToConvert, resultType: change.target.value })}>
                                <option>Gramy</option>
                                <option>Mililitry</option>
                                <option>Łyżeczki</option>
                                <option>Łyżki</option>
                            </Form.Control>
                        </Form>
                        <h3 className="h4 text-uppercase font-weight-light mt-3 title">Wynik: </h3>
                        <div id="calculate-volume-output" className="calculate-volume-output form-control text-left">
                            {Convert()}
                            {/* {dataToConvert.outputAmount} */}
                        </div>
                    </ol>


                </div>
                <div className="col-4 text-center">
                    <Table borderless hover variant>
                        <thead>
                            <tr>
                                <th>Bez termoobiegu</th>
                                <th>Z termoobiegiem</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>110<span>&#8451;</span></td>
                                <td>90<span>&#8451;</span></td>
                            </tr>
                            <tr>
                                <td>120<span>&#8451;</span></td>
                                <td>100<span>&#8451;</span></td>
                            </tr>
                            <tr>
                                <td>140<span>&#8451;</span></td>
                                <td>120<span>&#8451;</span></td>
                            </tr>
                            <tr>
                                <td>160<span>&#8451;</span></td>
                                <td>140<span>&#8451;</span></td>
                            </tr>
                            <tr>
                                <td>180<span>&#8451;</span></td>
                                <td>160<span>&#8451;</span></td>
                            </tr>
                            <tr>
                                <td>190<span>&#8451;</span></td>
                                <td>170<span>&#8451;</span></td>
                            </tr>
                            <tr>
                                <td>200<span>&#8451;</span></td>
                                <td>180<span>&#8451;</span></td>
                            </tr>
                            <tr>
                                <td>220<span>&#8451;</span></td>
                                <td>200<span>&#8451;</span></td>
                            </tr>
                            <tr>
                                <td>230<span>&#8451;</span></td>
                                <td>210<span>&#8451;</span></td>
                            </tr>
                            <tr>
                                <td>240<span>&#8451;</span></td>
                                <td>220<span>&#8451;</span></td>
                            </tr>
                        </tbody>
                    </Table>
                </div>

            </div>


        </div>
    );
}



export default CalculateVolume;