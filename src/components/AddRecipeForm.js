import React, { useState } from 'react';
import './AddRecipeForm.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

function AddRecipeForm() {
    const [recipeToAdd, setRecipeToAdd] = useState({
        categories: [],
        name: null,
        link: null,
        eatingDate: null,
        comment: null,
    })


    const { categories, name, link, comment, eatingDate } = recipeToAdd;

    const handleCategoryChange = (category) => {
        const newCategories = categories.includes(category) ? categories.filter((elem) => elem !== category) : [...categories, category];
        setRecipeToAdd({ ...recipeToAdd, categories: newCategories });
    }
    const handleButtonClick = () => {

        const data = {
            categories: categories,
            name: name,
            link: link,
            eatingDate: eatingDate,
            comment: comment,
        }
        fetch('http://192.168.1.68:5000/recipe', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .catch(error => console.error(error));
    }

    return (
        <div className="row pt-5">
            <div className="col-4"></div>
            <div className="text-center add-recipe col-md-4 mx-2">
                <h3 className="mb-3">Dodaj własne danie</h3>
                <Form className="row">
                    <div className="col-md-4" />
                    <div className="col-md-4 col-sm-8 text-center">
                        <Form.Check type="checkbox" label="Śniadanie" onChange={() => handleCategoryChange('breakfast')} />
                        <Form.Check type="checkbox" label="Lunch" onChange={() => handleCategoryChange('lunch')} />
                        <Form.Check type="checkbox" label="Obiad" onChange={() => handleCategoryChange('dinner')} />
                        <Form.Check type="checkbox" label="Deser" onChange={() => handleCategoryChange('dessert')} />
                        <Form.Check type="checkbox" label="Kolacja" onChange={() => handleCategoryChange('supper')} />
                        <Form.Check type="checkbox" label="Wypieki" onChange={() => handleCategoryChange('pastries')} />
                    </div>
                    <div className="col-md-4" />
                    <Form.Group controlId="formName" className="mt-4 px-5 col-12">
                        <Form.Label>Podaj nazwę dania:</Form.Label>
                        <Form.Control type="text" placeholder="nazwa" onChange={(event) => setRecipeToAdd({ ...recipeToAdd, name: event.target.value })} />
                    </Form.Group>

                    <Form.Group controlId="formLink" className="mt-4 px-5 col-12">
                        <Form.Label>Podaj link:</Form.Label>
                        <Form.Control type="text" placeholder="link" onChange={(event) => setRecipeToAdd({ ...recipeToAdd, link: event.target.value })} />
                    </Form.Group>
                    <Form.Group controlId="formEatingDate" className="mt-4 px-5 col-12">
                        <Form.Label>Data posiłku: </Form.Label>
                        <Form.Control type="date" className="pl-5" onChange={(event) => setRecipeToAdd({...recipeToAdd, eatingDate: event.target.value})}/>
                    </Form.Group>
                    <Form.Group controlId="formName" className="mt-4 px-5 col-12">
                        <Form.Label>Komentarz (opcjonalnie):</Form.Label>
                        <Form.Control type="text" placeholder="komentarz" onChange={(event) => setRecipeToAdd({ ...recipeToAdd, comment: event.target.value })} />
                    </Form.Group>
                    <div className="col-4" />
                    <Button variant="secondary" className="mt-4 col-4" onClick={handleButtonClick}>
                        Zatwierdź
                    </Button>
                    <div className="col-4" />
                </Form>
            </div>
        </div>
    );
}
export default AddRecipeForm;